//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Tue Aug  6 13:28:12 2019 by ROOT version 6.19/01
// from TTree tree_NOMINAL/output tree
// found on file: tree_diboson_uncut.root
//////////////////////////////////////////////////////////

#ifndef tree_diboson_h
#define tree_diboson_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"
#include "vector"
#include "vector"
#include "TVectorT.h"
#include "TMatrixT.h"

class tree_diboson {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Int_t           EventInfo_RunNumber;
   Int_t           MetaData_IsFullSim;
   Int_t           EventInfo_ChannelNumber;
   Int_t           EventInfo_LumiBlock;
   ULong64_t       EventInfo_EventNumber;
   Int_t           EventInfo_npv;
   Int_t           EventInfo_RndRunNumber;
   Float_t         Event_AverageMu;
   Float_t         Event_ActualMu;
   Float_t         Event_CorrAverageMu;
   Float_t         Event_CorrActualMu;
   Float_t         Event_RandomVal;
   Float_t         Truth_Boson_Mass;
   vector<string>  *trigList;
   vector<int>     *trigPass;
   vector<float>   *Muons_Eta;
   vector<float>   *Muons_PT;
   vector<int>     *Muons_quality;
   vector<int>     *MatchedMuon;
   Int_t           Hmumu;
   Int_t           Zmumu;
   Int_t           EventInfo_PassTrigger;
   Int_t           EventInfo_PassTriggerMatching;
   Int_t           Trigger;
   Int_t           TriggerMatch;
   Int_t           Pt;
   Int_t           OS;
   Int_t           Mass;
   Int_t           MET;
   Int_t           Bveto;
   Int_t           Category;
   Int_t           Muons_Charge_Lead;
   Int_t           Muons_Charge_Sub;
   Int_t           Muons_Type_Lead;
   Int_t           Muons_Type_Sub;
   Int_t           Muons_PassLoose_Lead;
   Int_t           Muons_PassLoose_Sub;
   Int_t           Muons_PassMedium_Lead;
   Int_t           Muons_PassMedium_Sub;
   Int_t           Muons_Multip;
   Int_t           Muons_TruthOrigin_Lead;
   Int_t           Muons_TruthOrigin_Sub;
   Int_t           Muons_TruthType_Lead;
   Int_t           Muons_TruthType_Sub;
   Float_t         Muons_Minv_MuMu;
   Float_t         Muons_PT_Lead;
   Float_t         Muons_PT_Sub;
   Float_t         Muons_Eta_Lead;
   Float_t         Muons_Eta_Sub;
   Float_t         Muons_Phi_Lead;
   Float_t         Muons_Phi_Sub;
   Float_t         Muons_d0sig_Lead;
   Float_t         Muons_d0sig_Sub;
   Float_t         Muons_z0_Lead;
   Float_t         Muons_z0_Sub;
   Float_t         Muons_MCPExpReso_NoSmear_Lead;
   Float_t         Muons_MCPExpReso_NoSmear_Sub;
   Float_t         Muons_MCPExpReso_NoSmear_Minv_MuMu_Sigma;
   Float_t         Muons_MCPExpReso_Smear_Lead;
   Float_t         Muons_MCPExpReso_Smear_Sub;
   Float_t         Muons_MCPExpReso_Smear_Minv_MuMu_Sigma;
   Float_t         Muons_Minv_MuMu_Fsr;
   Float_t         Muons_Minv_MuMu_Sigma;
   Float_t         Muons_Minv_MuMu_Fsr_Sigma;
   Float_t         Muons_PT_over_Minv_Lead;
   Float_t         Muons_PT_over_Minv_Sub;
   Float_t         Muons_DeltaEta_MuMu;
   Float_t         Muons_DeltaPhi_MuMu;
   Float_t         Muons_DeltaR_MuMu;
   Float_t         Muons_CosThetaStar;
   Float_t         Muons_mass_reso;
   vector<float>   *Muons_Add_PT;
   vector<float>   *Muons_Add_Eta;
   vector<float>   *Muons_Add_Phi;
   vector<float>   *Muons_Add_E;
   vector<float>   *Muons_Add_Weight;
   vector<int>     *Muons_Add_Charge;
   vector<int>     *Muons_Add_Type;
   vector<float>   *Muons_Pos_PT;
   vector<float>   *Muons_Pos_Eta;
   vector<float>   *Muons_Pos_Phi;
   vector<float>   *Muons_Pos_E;
   vector<float>   *Muons_Neg_PT;
   vector<float>   *Muons_Neg_Eta;
   vector<float>   *Muons_Neg_Phi;
   vector<float>   *Muons_Neg_E;
   vector<float>   *Electrons_Pos_PT;
   vector<float>   *Electrons_Pos_Eta;
   vector<float>   *Electrons_Pos_Phi;
   vector<float>   *Electrons_Pos_E;
   vector<float>   *Electrons_Neg_PT;
   vector<float>   *Electrons_Neg_Eta;
   vector<float>   *Electrons_Neg_Phi;
   vector<float>   *Electrons_Neg_E;
   Float_t         FSR_Et;
   Float_t         FSR_Eta;
   Float_t         FSR_Phi;
   Float_t         FSR_f1;
   Float_t         FSR_DeltaR;
   Int_t           FSR_Type;
   Int_t           FSR_IsPhoton;
   Int_t           Electrons_Multip;
   vector<float>   *Electrons_PT;
   vector<float>   *Electrons_Eta;
   vector<float>   *Electrons_Phi;
   vector<float>   *Electrons_E;
   vector<int>     *Electrons_Charge;
   Float_t         Z_PT;
   Float_t         Z_Y;
   Float_t         Z_Eta;
   Float_t         Z_Phi;
   Float_t         Z_PT_FSR;
   Float_t         Z_Y_FSR;
   Float_t         Z_Eta_FSR;
   Float_t         Z_Phi_FSR;
   Float_t         Z_PT_over_Minv;
   Float_t         Z_PT_over_Minv_FSR;
   Float_t         Event_MET;
   Float_t         Event_MET_Phi;
   Float_t         Event_MET_Sig;
   Float_t         Event_Ht;
   Int_t           Event_HasBJet;
   Int_t           Event_OldCategory;
   Float_t         Truth_PT_Lead_Muon;
   Float_t         Truth_PT_Sub_Muon;
   Float_t         Truth_Eta_Lead_Muon;
   Float_t         Truth_Eta_Sub_Muon;
   Float_t         Truth_Phi_Lead_Muon;
   Float_t         Truth_Phi_Sub_Muon;
   Int_t           Truth_Status_Lead_Muon;
   Int_t           Truth_Status_Sub_Muon;
   Float_t         Truth_Minv_MuMu;
   Float_t         Truth_QoverP_Lead_Muon;
   Float_t         Truth_QoverP_Sub_Muon;
   Float_t         TriggerSFWeight;
   Float_t         GlobalWeight;
   Float_t         EventWeight_PileupWeight;
   Float_t         MuonSFWeight;
   vector<float>   *ElectronSFWeight;
   Float_t         JetSFWeight;
   Float_t         Jvt_weight;
   Float_t         TotalWeight;
   Float_t         EventWeight_MCEventWeight;
   Float_t         Normal_factor;
   Float_t         SF_Zpt;
   Float_t         SF_mjj;
   Int_t           Jets_jetMultip;
   vector<float>   *Jets_PT;
   vector<float>   *Jets_Eta;
   vector<float>   *Jets_Phi;
   vector<float>   *Jets_E;
   vector<int>     *Jets_bjet;
   vector<int>     *Jets_PassFJVT;
   vector<int>     *Jets_LowestPassedBTagOP;
   Float_t         Jets_Minv_jj;
   Float_t         Jets_PT_jj;
   Float_t         Jets_PT_Lead;
   Float_t         Jets_PT_Sub;
   Float_t         Jets_Eta_Lead;
   Float_t         Jets_Eta_Sub;
   Float_t         Jets_Phi_Lead;
   Float_t         Jets_Phi_Sub;
   Float_t         Jets_E_Lead;
   Float_t         Jets_E_Sub;
   Int_t           Jets_PassFJVT_Lead;
   Int_t           Jets_PassFJVT_Sub;
   Float_t         Jets_FJVT_Lead;
   Float_t         Jets_FJVT_Sub;
   Float_t         Jets_JVT_Lead;
   Float_t         Jets_JVT_Sub;
   Float_t         Jets_TruthDR_Lead;
   Float_t         Jets_TruthDR_Sub;
   Float_t         Jets_DeltaR_jj;
   Float_t         Jets_DeltaEta_jj;
   Float_t         Jets_DeltaRap_jj;
   Float_t         Jets_DeltaPhi_jj;
   Float_t         Jets_etaj1_x_etaj2;
   Float_t         Jets_Eta_jj;
   Float_t         Jets_Phi_jj;
   Float_t         Event_Centrality;
   Float_t         Event_PT_MuMuj1;
   Float_t         Event_PT_MuMuj2;
   Float_t         Event_PT_MuMujj;
   Float_t         Event_Y_MuMuj1;
   Float_t         Event_Y_MuMuj2;
   Float_t         Event_Y_MuMujj;
   Float_t         Event_bdt_output;
   TVectorD        *Muons_CombFit_FullVec_Lead;
   TVectorD        *Muons_CombFit_FullVec_Sub;
   TMatrixD        *Muons_CombFit_FullCov_Lead;
   TMatrixD        *Muons_CombFit_FullCov_Sub;
   Float_t         Muons_CombFit_Lead_QoverP;
   Float_t         Muons_CombFit_Sub_QoverP;
   Float_t         Muons_CombFit_Lead_QoverP_Sigma;
   Float_t         Muons_CombFit_Sub_QoverP_Sigma;
   Float_t         Muons_CombFit_Minv_MuMu;
   Float_t         Muons_CombFit_Minv_MuMu_Sigma;
   Float_t         Muons_CombFit_Lead_Smear_Reso;
   Float_t         Muons_CombFit_Sub_Smear_Reso;
   Float_t         Muons_CombFit_Minv_MuMu_Smear_Sigma;
   Double_t        weightr;

   // List of branches
   TBranch        *b_EventInfo_RunNumber;   //!
   TBranch        *b_MetaData_IsFullSim;   //!
   TBranch        *b_EventInfo_ChannelNumber;   //!
   TBranch        *b_EventInfo_LumiBlock;   //!
   TBranch        *b_EventInfo_EventNumber;   //!
   TBranch        *b_EventInfo_npv;   //!
   TBranch        *b_EventInfo_RndRunNumber;   //!
   TBranch        *b_Event_AverageMu;   //!
   TBranch        *b_Event_ActualMu;   //!
   TBranch        *b_Event_CorrAverageMu;   //!
   TBranch        *b_Event_CorrActualMu;   //!
   TBranch        *b_Event_RandomVal;   //!
   TBranch        *b_Truth_Boson_Mass;   //!
   TBranch        *b_trigList;   //!
   TBranch        *b_trigPass;   //!
   TBranch        *b_Muons_Eta;   //!
   TBranch        *b_Muons_PT;   //!
   TBranch        *b_Muons_quality;   //!
   TBranch        *b_MatchedMuon;   //!
   TBranch        *b_Hmumu;   //!
   TBranch        *b_Zmumu;   //!
   TBranch        *b_EventInfo_PassTrigger;   //!
   TBranch        *b_EventInfo_PassTriggerMatching;   //!
   TBranch        *b_Trigger;   //!
   TBranch        *b_TriggerMatch;   //!
   TBranch        *b_Pt;   //!
   TBranch        *b_OS;   //!
   TBranch        *b_Mass;   //!
   TBranch        *b_MET;   //!
   TBranch        *b_Bveto;   //!
   TBranch        *b_Category;   //!
   TBranch        *b_Muons_Charge_Lead;   //!
   TBranch        *b_Muons_Charge_Sub;   //!
   TBranch        *b_Muons_Type_Lead;   //!
   TBranch        *b_Muons_Type_Sub;   //!
   TBranch        *b_Muons_PassLoose_Lead;   //!
   TBranch        *b_Muons_PassLoose_Sub;   //!
   TBranch        *b_Muons_PassMedium_Lead;   //!
   TBranch        *b_Muons_PassMedium_Sub;   //!
   TBranch        *b_Muons_Multip;   //!
   TBranch        *b_Muons_TruthOrigin_Lead;   //!
   TBranch        *b_Muons_TruthOrigin_Sub;   //!
   TBranch        *b_Muons_TruthType_Lead;   //!
   TBranch        *b_Muons_TruthType_Sub;   //!
   TBranch        *b_Muons_Minv_MuMu;   //!
   TBranch        *b_Muons_PT_Lead;   //!
   TBranch        *b_Muons_PT_Sub;   //!
   TBranch        *b_Muons_Eta_Lead;   //!
   TBranch        *b_Muons_Eta_Sub;   //!
   TBranch        *b_Muons_Phi_Lead;   //!
   TBranch        *b_Muons_Phi_Sub;   //!
   TBranch        *b_Muons_d0sig_Lead;   //!
   TBranch        *b_Muons_d0sig_Sub;   //!
   TBranch        *b_Muons_z0_Lead;   //!
   TBranch        *b_Muons_z0_Sub;   //!
   TBranch        *b_Muons_MCPExpReso_NoSmear_Lead;   //!
   TBranch        *b_Muons_MCPExpReso_NoSmear_Sub;   //!
   TBranch        *b_Muons_MCPExpReso_NoSmear_Minv_MuMu_Sigma;   //!
   TBranch        *b_Muons_MCPExpReso_Smear_Lead;   //!
   TBranch        *b_Muons_MCPExpReso_Smear_Sub;   //!
   TBranch        *b_Muons_MCPExpReso_Smear_Minv_MuMu_Sigma;   //!
   TBranch        *b_Muons_Minv_MuMu_Fsr;   //!
   TBranch        *b_Muons_Minv_MuMu_Sigma;   //!
   TBranch        *b_Muons_Minv_MuMu_Fsr_Sigma;   //!
   TBranch        *b_Muons_PT_over_Minv_Lead;   //!
   TBranch        *b_Muons_PT_over_Minv_Sub;   //!
   TBranch        *b_Muons_DeltaEta_MuMu;   //!
   TBranch        *b_Muons_DeltaPhi_MuMu;   //!
   TBranch        *b_Muons_DeltaR_MuMu;   //!
   TBranch        *b_Muons_CosThetaStar;   //!
   TBranch        *b_Muons_mass_reso;   //!
   TBranch        *b_Muons_Add_PT;   //!
   TBranch        *b_Muons_Add_Eta;   //!
   TBranch        *b_Muons_Add_Phi;   //!
   TBranch        *b_Muons_Add_E;   //!
   TBranch        *b_Muons_Add_Weight;   //!
   TBranch        *b_Muons_Add_Charge;   //!
   TBranch        *b_Muons_Add_Type;   //!
   TBranch        *b_Muons_Pos_PT;   //!
   TBranch        *b_Muons_Pos_Eta;   //!
   TBranch        *b_Muons_Pos_Phi;   //!
   TBranch        *b_Muons_Pos_E;   //!
   TBranch        *b_Muons_Neg_PT;   //!
   TBranch        *b_Muons_Neg_Eta;   //!
   TBranch        *b_Muons_Neg_Phi;   //!
   TBranch        *b_Muons_Neg_E;   //!
   TBranch        *b_Electrons_Pos_PT;   //!
   TBranch        *b_Electrons_Pos_Eta;   //!
   TBranch        *b_Electrons_Pos_Phi;   //!
   TBranch        *b_Electrons_Pos_E;   //!
   TBranch        *b_Electrons_Neg_PT;   //!
   TBranch        *b_Electrons_Neg_Eta;   //!
   TBranch        *b_Electrons_Neg_Phi;   //!
   TBranch        *b_Electrons_Neg_E;   //!
   TBranch        *b_FSR_Et;   //!
   TBranch        *b_FSR_Eta;   //!
   TBranch        *b_FSR_Phi;   //!
   TBranch        *b_FSR_f1;   //!
   TBranch        *b_FSR_DeltaR;   //!
   TBranch        *b_FSR_Type;   //!
   TBranch        *b_FSR_IsPhoton;   //!
   TBranch        *b_Electrons_Multip;   //!
   TBranch        *b_Electrons_PT;   //!
   TBranch        *b_Electrons_Eta;   //!
   TBranch        *b_Electrons_Phi;   //!
   TBranch        *b_Electrons_E;   //!
   TBranch        *b_Electrons_Charge;   //!
   TBranch        *b_Z_PT;   //!
   TBranch        *b_Z_Y;   //!
   TBranch        *b_Z_Eta;   //!
   TBranch        *b_Z_Phi;   //!
   TBranch        *b_Z_PT_FSR;   //!
   TBranch        *b_Z_Y_FSR;   //!
   TBranch        *b_Z_Eta_FSR;   //!
   TBranch        *b_Z_Phi_FSR;   //!
   TBranch        *b_Z_PT_over_Minv;   //!
   TBranch        *b_Z_PT_over_Minv_FSR;   //!
   TBranch        *b_Event_MET;   //!
   TBranch        *b_Event_MET_Phi;   //!
   TBranch        *b_Event_MET_Sig;   //!
   TBranch        *b_Event_Ht;   //!
   TBranch        *b_Event_HasBJet;   //!
   TBranch        *b_Event_OldCategory;   //!
   TBranch        *b_Truth_PT_Lead_Muon;   //!
   TBranch        *b_Truth_PT_Sub_Muon;   //!
   TBranch        *b_Truth_Eta_Lead_Muon;   //!
   TBranch        *b_Truth_Eta_Sub_Muon;   //!
   TBranch        *b_Truth_Phi_Lead_Muon;   //!
   TBranch        *b_Truth_Phi_Sub_Muon;   //!
   TBranch        *b_Truth_Status_Lead_Muon;   //!
   TBranch        *b_Truth_Status_Sub_Muon;   //!
   TBranch        *b_Truth_Minv_MuMu;   //!
   TBranch        *b_Truth_QoverP_Lead_Muon;   //!
   TBranch        *b_Truth_QoverP_Sub_Muon;   //!
   TBranch        *b_TriggerSFWeight;   //!
   TBranch        *b_GlobalWeight;   //!
   TBranch        *b_EventWeight_PileupWeight;   //!
   TBranch        *b_MuonSFWeight;   //!
   TBranch        *b_ElectronSFWeight;   //!
   TBranch        *b_JetSFWeight;   //!
   TBranch        *b_Jvt_weight;   //!
   TBranch        *b_TotalWeight;   //!
   TBranch        *b_EventWeight_MCEventWeight;   //!
   TBranch        *b_Normal_factor;   //!
   TBranch        *b_SF_Zpt;   //!
   TBranch        *b_SF_mjj;   //!
   TBranch        *b_Jets_jetMultip;   //!
   TBranch        *b_Jets_PT;   //!
   TBranch        *b_Jets_Eta;   //!
   TBranch        *b_Jets_Phi;   //!
   TBranch        *b_Jets_E;   //!
   TBranch        *b_Jets_bjet;   //!
   TBranch        *b_Jets_PassFJVT;   //!
   TBranch        *b_Jets_LowestPassedBTagOP;   //!
   TBranch        *b_Jets_Minv_jj;   //!
   TBranch        *b_Jets_PT_jj;   //!
   TBranch        *b_Jets_PT_Lead;   //!
   TBranch        *b_Jets_PT_Sub;   //!
   TBranch        *b_Jets_Eta_Lead;   //!
   TBranch        *b_Jets_Eta_Sub;   //!
   TBranch        *b_Jets_Phi_Lead;   //!
   TBranch        *b_Jets_Phi_Sub;   //!
   TBranch        *b_Jets_E_Lead;   //!
   TBranch        *b_Jets_E_Sub;   //!
   TBranch        *b_Jets_PassFJVT_Lead;   //!
   TBranch        *b_Jets_PassFJVT_Sub;   //!
   TBranch        *b_Jets_FJVT_Lead;   //!
   TBranch        *b_Jets_FJVT_Sub;   //!
   TBranch        *b_Jets_JVT_Lead;   //!
   TBranch        *b_Jets_JVT_Sub;   //!
   TBranch        *b_Jets_TruthDR_Lead;   //!
   TBranch        *b_Jets_TruthDR_Sub;   //!
   TBranch        *b_Jets_DeltaR_jj;   //!
   TBranch        *b_Jets_DeltaEta_jj;   //!
   TBranch        *b_Jets_DeltaRap_jj;   //!
   TBranch        *b_Jets_DeltaPhi_jj;   //!
   TBranch        *b_Jets_etaj1_x_etaj2;   //!
   TBranch        *b_Jets_Eta_jj;   //!
   TBranch        *b_Jets_Phi_jj;   //!
   TBranch        *b_Event_Centrality;   //!
   TBranch        *b_Event_PT_MuMuj1;   //!
   TBranch        *b_Event_PT_MuMuj2;   //!
   TBranch        *b_Event_PT_MuMujj;   //!
   TBranch        *b_Event_Y_MuMuj1;   //!
   TBranch        *b_Event_Y_MuMuj2;   //!
   TBranch        *b_Event_Y_MuMujj;   //!
   TBranch        *b_Event_bdt_output;   //!
   TBranch        *b_Muons_CombFit_FullVec_Lead;   //!
   TBranch        *b_Muons_CombFit_FullVec_Sub;   //!
   TBranch        *b_Muons_CombFit_FullCov_Lead;   //!
   TBranch        *b_Muons_CombFit_FullCov_Sub;   //!
   TBranch        *b_Muons_CombFit_Lead_QoverP;   //!
   TBranch        *b_Muons_CombFit_Sub_QoverP;   //!
   TBranch        *b_Muons_CombFit_Lead_QoverP_Sigma;   //!
   TBranch        *b_Muons_CombFit_Sub_QoverP_Sigma;   //!
   TBranch        *b_Muons_CombFit_Minv_MuMu;   //!
   TBranch        *b_Muons_CombFit_Minv_MuMu_Sigma;   //!
   TBranch        *b_Muons_CombFit_Lead_Smear_Reso;   //!
   TBranch        *b_Muons_CombFit_Sub_Smear_Reso;   //!
   TBranch        *b_Muons_CombFit_Minv_MuMu_Smear_Sigma;   //!
   TBranch        *b_weightr;   //!

   tree_diboson(TTree *tree=0);
   virtual ~tree_diboson();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef tree_diboson_cxx
tree_diboson::tree_diboson(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("tree_Wt_uncut.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("tree_Wt_uncut.root");
      }
      f->GetObject("tree_NOMINAL",tree);

   }
   Init(tree);
}

tree_diboson::~tree_diboson()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t tree_diboson::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t tree_diboson::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void tree_diboson::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   trigList = 0;
   trigPass = 0;
   Muons_Eta = 0;
   Muons_PT = 0;
   Muons_quality = 0;
   MatchedMuon = 0;
   Muons_Add_PT = 0;
   Muons_Add_Eta = 0;
   Muons_Add_Phi = 0;
   Muons_Add_E = 0;
   Muons_Add_Weight = 0;
   Muons_Add_Charge = 0;
   Muons_Add_Type = 0;
   Muons_Pos_PT = 0;
   Muons_Pos_Eta = 0;
   Muons_Pos_Phi = 0;
   Muons_Pos_E = 0;
   Muons_Neg_PT = 0;
   Muons_Neg_Eta = 0;
   Muons_Neg_Phi = 0;
   Muons_Neg_E = 0;
   Electrons_Pos_PT = 0;
   Electrons_Pos_Eta = 0;
   Electrons_Pos_Phi = 0;
   Electrons_Pos_E = 0;
   Electrons_Neg_PT = 0;
   Electrons_Neg_Eta = 0;
   Electrons_Neg_Phi = 0;
   Electrons_Neg_E = 0;
   Electrons_PT = 0;
   Electrons_Eta = 0;
   Electrons_Phi = 0;
   Electrons_E = 0;
   Electrons_Charge = 0;
   ElectronSFWeight = 0;
   Jets_PT = 0;
   Jets_Eta = 0;
   Jets_Phi = 0;
   Jets_E = 0;
   Jets_bjet = 0;
   Jets_PassFJVT = 0;
   Jets_LowestPassedBTagOP = 0;
   Muons_CombFit_FullVec_Lead = 0;
   Muons_CombFit_FullVec_Sub = 0;
   Muons_CombFit_FullCov_Lead = 0;
   Muons_CombFit_FullCov_Sub = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("EventInfo_RunNumber", &EventInfo_RunNumber, &b_EventInfo_RunNumber);
   fChain->SetBranchAddress("MetaData_IsFullSim", &MetaData_IsFullSim, &b_MetaData_IsFullSim);
   fChain->SetBranchAddress("EventInfo_ChannelNumber", &EventInfo_ChannelNumber, &b_EventInfo_ChannelNumber);
   fChain->SetBranchAddress("EventInfo_LumiBlock", &EventInfo_LumiBlock, &b_EventInfo_LumiBlock);
   fChain->SetBranchAddress("EventInfo_EventNumber", &EventInfo_EventNumber, &b_EventInfo_EventNumber);
   fChain->SetBranchAddress("EventInfo_npv", &EventInfo_npv, &b_EventInfo_npv);
   fChain->SetBranchAddress("EventInfo_RndRunNumber", &EventInfo_RndRunNumber, &b_EventInfo_RndRunNumber);
   fChain->SetBranchAddress("Event_AverageMu", &Event_AverageMu, &b_Event_AverageMu);
   fChain->SetBranchAddress("Event_ActualMu", &Event_ActualMu, &b_Event_ActualMu);
   fChain->SetBranchAddress("Event_CorrAverageMu", &Event_CorrAverageMu, &b_Event_CorrAverageMu);
   fChain->SetBranchAddress("Event_CorrActualMu", &Event_CorrActualMu, &b_Event_CorrActualMu);
   fChain->SetBranchAddress("Event_RandomVal", &Event_RandomVal, &b_Event_RandomVal);
   fChain->SetBranchAddress("Truth_Boson_Mass", &Truth_Boson_Mass, &b_Truth_Boson_Mass);
   fChain->SetBranchAddress("trigList", &trigList, &b_trigList);
   fChain->SetBranchAddress("trigPass", &trigPass, &b_trigPass);
   fChain->SetBranchAddress("Muons_Eta", &Muons_Eta, &b_Muons_Eta);
   fChain->SetBranchAddress("Muons_PT", &Muons_PT, &b_Muons_PT);
   fChain->SetBranchAddress("Muons_quality", &Muons_quality, &b_Muons_quality);
   fChain->SetBranchAddress("MatchedMuon", &MatchedMuon, &b_MatchedMuon);
   fChain->SetBranchAddress("Hmumu", &Hmumu, &b_Hmumu);
   fChain->SetBranchAddress("Zmumu", &Zmumu, &b_Zmumu);
   fChain->SetBranchAddress("EventInfo_PassTrigger", &EventInfo_PassTrigger, &b_EventInfo_PassTrigger);
   fChain->SetBranchAddress("EventInfo_PassTriggerMatching", &EventInfo_PassTriggerMatching, &b_EventInfo_PassTriggerMatching);
   fChain->SetBranchAddress("Trigger", &Trigger, &b_Trigger);
   fChain->SetBranchAddress("TriggerMatch", &TriggerMatch, &b_TriggerMatch);
   fChain->SetBranchAddress("Pt", &Pt, &b_Pt);
   fChain->SetBranchAddress("OS", &OS, &b_OS);
   fChain->SetBranchAddress("Mass", &Mass, &b_Mass);
   fChain->SetBranchAddress("MET", &MET, &b_MET);
   fChain->SetBranchAddress("Bveto", &Bveto, &b_Bveto);
   fChain->SetBranchAddress("Category", &Category, &b_Category);
   fChain->SetBranchAddress("Muons_Charge_Lead", &Muons_Charge_Lead, &b_Muons_Charge_Lead);
   fChain->SetBranchAddress("Muons_Charge_Sub", &Muons_Charge_Sub, &b_Muons_Charge_Sub);
   fChain->SetBranchAddress("Muons_Type_Lead", &Muons_Type_Lead, &b_Muons_Type_Lead);
   fChain->SetBranchAddress("Muons_Type_Sub", &Muons_Type_Sub, &b_Muons_Type_Sub);
   fChain->SetBranchAddress("Muons_PassLoose_Lead", &Muons_PassLoose_Lead, &b_Muons_PassLoose_Lead);
   fChain->SetBranchAddress("Muons_PassLoose_Sub", &Muons_PassLoose_Sub, &b_Muons_PassLoose_Sub);
   fChain->SetBranchAddress("Muons_PassMedium_Lead", &Muons_PassMedium_Lead, &b_Muons_PassMedium_Lead);
   fChain->SetBranchAddress("Muons_PassMedium_Sub", &Muons_PassMedium_Sub, &b_Muons_PassMedium_Sub);
   fChain->SetBranchAddress("Muons_Multip", &Muons_Multip, &b_Muons_Multip);
   fChain->SetBranchAddress("Muons_TruthOrigin_Lead", &Muons_TruthOrigin_Lead, &b_Muons_TruthOrigin_Lead);
   fChain->SetBranchAddress("Muons_TruthOrigin_Sub", &Muons_TruthOrigin_Sub, &b_Muons_TruthOrigin_Sub);
   fChain->SetBranchAddress("Muons_TruthType_Lead", &Muons_TruthType_Lead, &b_Muons_TruthType_Lead);
   fChain->SetBranchAddress("Muons_TruthType_Sub", &Muons_TruthType_Sub, &b_Muons_TruthType_Sub);
   fChain->SetBranchAddress("Muons_Minv_MuMu", &Muons_Minv_MuMu, &b_Muons_Minv_MuMu);
   fChain->SetBranchAddress("Muons_PT_Lead", &Muons_PT_Lead, &b_Muons_PT_Lead);
   fChain->SetBranchAddress("Muons_PT_Sub", &Muons_PT_Sub, &b_Muons_PT_Sub);
   fChain->SetBranchAddress("Muons_Eta_Lead", &Muons_Eta_Lead, &b_Muons_Eta_Lead);
   fChain->SetBranchAddress("Muons_Eta_Sub", &Muons_Eta_Sub, &b_Muons_Eta_Sub);
   fChain->SetBranchAddress("Muons_Phi_Lead", &Muons_Phi_Lead, &b_Muons_Phi_Lead);
   fChain->SetBranchAddress("Muons_Phi_Sub", &Muons_Phi_Sub, &b_Muons_Phi_Sub);
   fChain->SetBranchAddress("Muons_d0sig_Lead", &Muons_d0sig_Lead, &b_Muons_d0sig_Lead);
   fChain->SetBranchAddress("Muons_d0sig_Sub", &Muons_d0sig_Sub, &b_Muons_d0sig_Sub);
   fChain->SetBranchAddress("Muons_z0_Lead", &Muons_z0_Lead, &b_Muons_z0_Lead);
   fChain->SetBranchAddress("Muons_z0_Sub", &Muons_z0_Sub, &b_Muons_z0_Sub);
   fChain->SetBranchAddress("Muons_MCPExpReso_NoSmear_Lead", &Muons_MCPExpReso_NoSmear_Lead, &b_Muons_MCPExpReso_NoSmear_Lead);
   fChain->SetBranchAddress("Muons_MCPExpReso_NoSmear_Sub", &Muons_MCPExpReso_NoSmear_Sub, &b_Muons_MCPExpReso_NoSmear_Sub);
   fChain->SetBranchAddress("Muons_MCPExpReso_NoSmear_Minv_MuMu_Sigma", &Muons_MCPExpReso_NoSmear_Minv_MuMu_Sigma, &b_Muons_MCPExpReso_NoSmear_Minv_MuMu_Sigma);
   fChain->SetBranchAddress("Muons_MCPExpReso_Smear_Lead", &Muons_MCPExpReso_Smear_Lead, &b_Muons_MCPExpReso_Smear_Lead);
   fChain->SetBranchAddress("Muons_MCPExpReso_Smear_Sub", &Muons_MCPExpReso_Smear_Sub, &b_Muons_MCPExpReso_Smear_Sub);
   fChain->SetBranchAddress("Muons_MCPExpReso_Smear_Minv_MuMu_Sigma", &Muons_MCPExpReso_Smear_Minv_MuMu_Sigma, &b_Muons_MCPExpReso_Smear_Minv_MuMu_Sigma);
   fChain->SetBranchAddress("Muons_Minv_MuMu_Fsr", &Muons_Minv_MuMu_Fsr, &b_Muons_Minv_MuMu_Fsr);
   fChain->SetBranchAddress("Muons_Minv_MuMu_Sigma", &Muons_Minv_MuMu_Sigma, &b_Muons_Minv_MuMu_Sigma);
   fChain->SetBranchAddress("Muons_Minv_MuMu_Fsr_Sigma", &Muons_Minv_MuMu_Fsr_Sigma, &b_Muons_Minv_MuMu_Fsr_Sigma);
   fChain->SetBranchAddress("Muons_PT_over_Minv_Lead", &Muons_PT_over_Minv_Lead, &b_Muons_PT_over_Minv_Lead);
   fChain->SetBranchAddress("Muons_PT_over_Minv_Sub", &Muons_PT_over_Minv_Sub, &b_Muons_PT_over_Minv_Sub);
   fChain->SetBranchAddress("Muons_DeltaEta_MuMu", &Muons_DeltaEta_MuMu, &b_Muons_DeltaEta_MuMu);
   fChain->SetBranchAddress("Muons_DeltaPhi_MuMu", &Muons_DeltaPhi_MuMu, &b_Muons_DeltaPhi_MuMu);
   fChain->SetBranchAddress("Muons_DeltaR_MuMu", &Muons_DeltaR_MuMu, &b_Muons_DeltaR_MuMu);
   fChain->SetBranchAddress("Muons_CosThetaStar", &Muons_CosThetaStar, &b_Muons_CosThetaStar);
   fChain->SetBranchAddress("Muons_mass_reso", &Muons_mass_reso, &b_Muons_mass_reso);
   fChain->SetBranchAddress("Muons_Add_PT", &Muons_Add_PT, &b_Muons_Add_PT);
   fChain->SetBranchAddress("Muons_Add_Eta", &Muons_Add_Eta, &b_Muons_Add_Eta);
   fChain->SetBranchAddress("Muons_Add_Phi", &Muons_Add_Phi, &b_Muons_Add_Phi);
   fChain->SetBranchAddress("Muons_Add_E", &Muons_Add_E, &b_Muons_Add_E);
   fChain->SetBranchAddress("Muons_Add_Weight", &Muons_Add_Weight, &b_Muons_Add_Weight);
   fChain->SetBranchAddress("Muons_Add_Charge", &Muons_Add_Charge, &b_Muons_Add_Charge);
   fChain->SetBranchAddress("Muons_Add_Type", &Muons_Add_Type, &b_Muons_Add_Type);
   fChain->SetBranchAddress("Muons_Pos_PT", &Muons_Pos_PT, &b_Muons_Pos_PT);
   fChain->SetBranchAddress("Muons_Pos_Eta", &Muons_Pos_Eta, &b_Muons_Pos_Eta);
   fChain->SetBranchAddress("Muons_Pos_Phi", &Muons_Pos_Phi, &b_Muons_Pos_Phi);
   fChain->SetBranchAddress("Muons_Pos_E", &Muons_Pos_E, &b_Muons_Pos_E);
   fChain->SetBranchAddress("Muons_Neg_PT", &Muons_Neg_PT, &b_Muons_Neg_PT);
   fChain->SetBranchAddress("Muons_Neg_Eta", &Muons_Neg_Eta, &b_Muons_Neg_Eta);
   fChain->SetBranchAddress("Muons_Neg_Phi", &Muons_Neg_Phi, &b_Muons_Neg_Phi);
   fChain->SetBranchAddress("Muons_Neg_E", &Muons_Neg_E, &b_Muons_Neg_E);
   fChain->SetBranchAddress("Electrons_Pos_PT", &Electrons_Pos_PT, &b_Electrons_Pos_PT);
   fChain->SetBranchAddress("Electrons_Pos_Eta", &Electrons_Pos_Eta, &b_Electrons_Pos_Eta);
   fChain->SetBranchAddress("Electrons_Pos_Phi", &Electrons_Pos_Phi, &b_Electrons_Pos_Phi);
   fChain->SetBranchAddress("Electrons_Pos_E", &Electrons_Pos_E, &b_Electrons_Pos_E);
   fChain->SetBranchAddress("Electrons_Neg_PT", &Electrons_Neg_PT, &b_Electrons_Neg_PT);
   fChain->SetBranchAddress("Electrons_Neg_Eta", &Electrons_Neg_Eta, &b_Electrons_Neg_Eta);
   fChain->SetBranchAddress("Electrons_Neg_Phi", &Electrons_Neg_Phi, &b_Electrons_Neg_Phi);
   fChain->SetBranchAddress("Electrons_Neg_E", &Electrons_Neg_E, &b_Electrons_Neg_E);
   fChain->SetBranchAddress("FSR_Et", &FSR_Et, &b_FSR_Et);
   fChain->SetBranchAddress("FSR_Eta", &FSR_Eta, &b_FSR_Eta);
   fChain->SetBranchAddress("FSR_Phi", &FSR_Phi, &b_FSR_Phi);
   fChain->SetBranchAddress("FSR_f1", &FSR_f1, &b_FSR_f1);
   fChain->SetBranchAddress("FSR_DeltaR", &FSR_DeltaR, &b_FSR_DeltaR);
   fChain->SetBranchAddress("FSR_Type", &FSR_Type, &b_FSR_Type);
   fChain->SetBranchAddress("FSR_IsPhoton", &FSR_IsPhoton, &b_FSR_IsPhoton);
   fChain->SetBranchAddress("Electrons_Multip", &Electrons_Multip, &b_Electrons_Multip);
   fChain->SetBranchAddress("Electrons_PT", &Electrons_PT, &b_Electrons_PT);
   fChain->SetBranchAddress("Electrons_Eta", &Electrons_Eta, &b_Electrons_Eta);
   fChain->SetBranchAddress("Electrons_Phi", &Electrons_Phi, &b_Electrons_Phi);
   fChain->SetBranchAddress("Electrons_E", &Electrons_E, &b_Electrons_E);
   fChain->SetBranchAddress("Electrons_Charge", &Electrons_Charge, &b_Electrons_Charge);
   fChain->SetBranchAddress("Z_PT", &Z_PT, &b_Z_PT);
   fChain->SetBranchAddress("Z_Y", &Z_Y, &b_Z_Y);
   fChain->SetBranchAddress("Z_Eta", &Z_Eta, &b_Z_Eta);
   fChain->SetBranchAddress("Z_Phi", &Z_Phi, &b_Z_Phi);
   fChain->SetBranchAddress("Z_PT_FSR", &Z_PT_FSR, &b_Z_PT_FSR);
   fChain->SetBranchAddress("Z_Y_FSR", &Z_Y_FSR, &b_Z_Y_FSR);
   fChain->SetBranchAddress("Z_Eta_FSR", &Z_Eta_FSR, &b_Z_Eta_FSR);
   fChain->SetBranchAddress("Z_Phi_FSR", &Z_Phi_FSR, &b_Z_Phi_FSR);
   fChain->SetBranchAddress("Z_PT_over_Minv", &Z_PT_over_Minv, &b_Z_PT_over_Minv);
   fChain->SetBranchAddress("Z_PT_over_Minv_FSR", &Z_PT_over_Minv_FSR, &b_Z_PT_over_Minv_FSR);
   fChain->SetBranchAddress("Event_MET", &Event_MET, &b_Event_MET);
   fChain->SetBranchAddress("Event_MET_Phi", &Event_MET_Phi, &b_Event_MET_Phi);
   fChain->SetBranchAddress("Event_MET_Sig", &Event_MET_Sig, &b_Event_MET_Sig);
   fChain->SetBranchAddress("Event_Ht", &Event_Ht, &b_Event_Ht);
   fChain->SetBranchAddress("Event_HasBJet", &Event_HasBJet, &b_Event_HasBJet);
   fChain->SetBranchAddress("Event_OldCategory", &Event_OldCategory, &b_Event_OldCategory);
   fChain->SetBranchAddress("Truth_PT_Lead_Muon", &Truth_PT_Lead_Muon, &b_Truth_PT_Lead_Muon);
   fChain->SetBranchAddress("Truth_PT_Sub_Muon", &Truth_PT_Sub_Muon, &b_Truth_PT_Sub_Muon);
   fChain->SetBranchAddress("Truth_Eta_Lead_Muon", &Truth_Eta_Lead_Muon, &b_Truth_Eta_Lead_Muon);
   fChain->SetBranchAddress("Truth_Eta_Sub_Muon", &Truth_Eta_Sub_Muon, &b_Truth_Eta_Sub_Muon);
   fChain->SetBranchAddress("Truth_Phi_Lead_Muon", &Truth_Phi_Lead_Muon, &b_Truth_Phi_Lead_Muon);
   fChain->SetBranchAddress("Truth_Phi_Sub_Muon", &Truth_Phi_Sub_Muon, &b_Truth_Phi_Sub_Muon);
   fChain->SetBranchAddress("Truth_Status_Lead_Muon", &Truth_Status_Lead_Muon, &b_Truth_Status_Lead_Muon);
   fChain->SetBranchAddress("Truth_Status_Sub_Muon", &Truth_Status_Sub_Muon, &b_Truth_Status_Sub_Muon);
   fChain->SetBranchAddress("Truth_Minv_MuMu", &Truth_Minv_MuMu, &b_Truth_Minv_MuMu);
   fChain->SetBranchAddress("Truth_QoverP_Lead_Muon", &Truth_QoverP_Lead_Muon, &b_Truth_QoverP_Lead_Muon);
   fChain->SetBranchAddress("Truth_QoverP_Sub_Muon", &Truth_QoverP_Sub_Muon, &b_Truth_QoverP_Sub_Muon);
   fChain->SetBranchAddress("TriggerSFWeight", &TriggerSFWeight, &b_TriggerSFWeight);
   fChain->SetBranchAddress("GlobalWeight", &GlobalWeight, &b_GlobalWeight);
   fChain->SetBranchAddress("EventWeight_PileupWeight", &EventWeight_PileupWeight, &b_EventWeight_PileupWeight);
   fChain->SetBranchAddress("MuonSFWeight", &MuonSFWeight, &b_MuonSFWeight);
   fChain->SetBranchAddress("ElectronSFWeight", &ElectronSFWeight, &b_ElectronSFWeight);
   fChain->SetBranchAddress("JetSFWeight", &JetSFWeight, &b_JetSFWeight);
   fChain->SetBranchAddress("Jvt_weight", &Jvt_weight, &b_Jvt_weight);
   fChain->SetBranchAddress("TotalWeight", &TotalWeight, &b_TotalWeight);
   fChain->SetBranchAddress("EventWeight_MCEventWeight", &EventWeight_MCEventWeight, &b_EventWeight_MCEventWeight);
   fChain->SetBranchAddress("Normal_factor", &Normal_factor, &b_Normal_factor);
   fChain->SetBranchAddress("SF_Zpt", &SF_Zpt, &b_SF_Zpt);
   fChain->SetBranchAddress("SF_mjj", &SF_mjj, &b_SF_mjj);
   fChain->SetBranchAddress("Jets_jetMultip", &Jets_jetMultip, &b_Jets_jetMultip);
   fChain->SetBranchAddress("Jets_PT", &Jets_PT, &b_Jets_PT);
   fChain->SetBranchAddress("Jets_Eta", &Jets_Eta, &b_Jets_Eta);
   fChain->SetBranchAddress("Jets_Phi", &Jets_Phi, &b_Jets_Phi);
   fChain->SetBranchAddress("Jets_E", &Jets_E, &b_Jets_E);
   fChain->SetBranchAddress("Jets_bjet", &Jets_bjet, &b_Jets_bjet);
   fChain->SetBranchAddress("Jets_PassFJVT", &Jets_PassFJVT, &b_Jets_PassFJVT);
   fChain->SetBranchAddress("Jets_LowestPassedBTagOP", &Jets_LowestPassedBTagOP, &b_Jets_LowestPassedBTagOP);
   fChain->SetBranchAddress("Jets_Minv_jj", &Jets_Minv_jj, &b_Jets_Minv_jj);
   fChain->SetBranchAddress("Jets_PT_jj", &Jets_PT_jj, &b_Jets_PT_jj);
   fChain->SetBranchAddress("Jets_PT_Lead", &Jets_PT_Lead, &b_Jets_PT_Lead);
   fChain->SetBranchAddress("Jets_PT_Sub", &Jets_PT_Sub, &b_Jets_PT_Sub);
   fChain->SetBranchAddress("Jets_Eta_Lead", &Jets_Eta_Lead, &b_Jets_Eta_Lead);
   fChain->SetBranchAddress("Jets_Eta_Sub", &Jets_Eta_Sub, &b_Jets_Eta_Sub);
   fChain->SetBranchAddress("Jets_Phi_Lead", &Jets_Phi_Lead, &b_Jets_Phi_Lead);
   fChain->SetBranchAddress("Jets_Phi_Sub", &Jets_Phi_Sub, &b_Jets_Phi_Sub);
   fChain->SetBranchAddress("Jets_E_Lead", &Jets_E_Lead, &b_Jets_E_Lead);
   fChain->SetBranchAddress("Jets_E_Sub", &Jets_E_Sub, &b_Jets_E_Sub);
   fChain->SetBranchAddress("Jets_PassFJVT_Lead", &Jets_PassFJVT_Lead, &b_Jets_PassFJVT_Lead);
   fChain->SetBranchAddress("Jets_PassFJVT_Sub", &Jets_PassFJVT_Sub, &b_Jets_PassFJVT_Sub);
   fChain->SetBranchAddress("Jets_FJVT_Lead", &Jets_FJVT_Lead, &b_Jets_FJVT_Lead);
   fChain->SetBranchAddress("Jets_FJVT_Sub", &Jets_FJVT_Sub, &b_Jets_FJVT_Sub);
   fChain->SetBranchAddress("Jets_JVT_Lead", &Jets_JVT_Lead, &b_Jets_JVT_Lead);
   fChain->SetBranchAddress("Jets_JVT_Sub", &Jets_JVT_Sub, &b_Jets_JVT_Sub);
   fChain->SetBranchAddress("Jets_TruthDR_Lead", &Jets_TruthDR_Lead, &b_Jets_TruthDR_Lead);
   fChain->SetBranchAddress("Jets_TruthDR_Sub", &Jets_TruthDR_Sub, &b_Jets_TruthDR_Sub);
   fChain->SetBranchAddress("Jets_DeltaR_jj", &Jets_DeltaR_jj, &b_Jets_DeltaR_jj);
   fChain->SetBranchAddress("Jets_DeltaEta_jj", &Jets_DeltaEta_jj, &b_Jets_DeltaEta_jj);
   fChain->SetBranchAddress("Jets_DeltaRap_jj", &Jets_DeltaRap_jj, &b_Jets_DeltaRap_jj);
   fChain->SetBranchAddress("Jets_DeltaPhi_jj", &Jets_DeltaPhi_jj, &b_Jets_DeltaPhi_jj);
   fChain->SetBranchAddress("Jets_etaj1_x_etaj2", &Jets_etaj1_x_etaj2, &b_Jets_etaj1_x_etaj2);
   fChain->SetBranchAddress("Jets_Eta_jj", &Jets_Eta_jj, &b_Jets_Eta_jj);
   fChain->SetBranchAddress("Jets_Phi_jj", &Jets_Phi_jj, &b_Jets_Phi_jj);
   fChain->SetBranchAddress("Event_Centrality", &Event_Centrality, &b_Event_Centrality);
   fChain->SetBranchAddress("Event_PT_MuMuj1", &Event_PT_MuMuj1, &b_Event_PT_MuMuj1);
   fChain->SetBranchAddress("Event_PT_MuMuj2", &Event_PT_MuMuj2, &b_Event_PT_MuMuj2);
   fChain->SetBranchAddress("Event_PT_MuMujj", &Event_PT_MuMujj, &b_Event_PT_MuMujj);
   fChain->SetBranchAddress("Event_Y_MuMuj1", &Event_Y_MuMuj1, &b_Event_Y_MuMuj1);
   fChain->SetBranchAddress("Event_Y_MuMuj2", &Event_Y_MuMuj2, &b_Event_Y_MuMuj2);
   fChain->SetBranchAddress("Event_Y_MuMujj", &Event_Y_MuMujj, &b_Event_Y_MuMujj);
   fChain->SetBranchAddress("Event_bdt_output", &Event_bdt_output, &b_Event_bdt_output);
   fChain->SetBranchAddress("Muons_CombFit_FullVec_Lead", &Muons_CombFit_FullVec_Lead, &b_Muons_CombFit_FullVec_Lead);
   fChain->SetBranchAddress("Muons_CombFit_FullVec_Sub", &Muons_CombFit_FullVec_Sub, &b_Muons_CombFit_FullVec_Sub);
   fChain->SetBranchAddress("Muons_CombFit_FullCov_Lead", &Muons_CombFit_FullCov_Lead, &b_Muons_CombFit_FullCov_Lead);
   fChain->SetBranchAddress("Muons_CombFit_FullCov_Sub", &Muons_CombFit_FullCov_Sub, &b_Muons_CombFit_FullCov_Sub);
   fChain->SetBranchAddress("Muons_CombFit_Lead_QoverP", &Muons_CombFit_Lead_QoverP, &b_Muons_CombFit_Lead_QoverP);
   fChain->SetBranchAddress("Muons_CombFit_Sub_QoverP", &Muons_CombFit_Sub_QoverP, &b_Muons_CombFit_Sub_QoverP);
   fChain->SetBranchAddress("Muons_CombFit_Lead_QoverP_Sigma", &Muons_CombFit_Lead_QoverP_Sigma, &b_Muons_CombFit_Lead_QoverP_Sigma);
   fChain->SetBranchAddress("Muons_CombFit_Sub_QoverP_Sigma", &Muons_CombFit_Sub_QoverP_Sigma, &b_Muons_CombFit_Sub_QoverP_Sigma);
   fChain->SetBranchAddress("Muons_CombFit_Minv_MuMu", &Muons_CombFit_Minv_MuMu, &b_Muons_CombFit_Minv_MuMu);
   fChain->SetBranchAddress("Muons_CombFit_Minv_MuMu_Sigma", &Muons_CombFit_Minv_MuMu_Sigma, &b_Muons_CombFit_Minv_MuMu_Sigma);
   fChain->SetBranchAddress("Muons_CombFit_Lead_Smear_Reso", &Muons_CombFit_Lead_Smear_Reso, &b_Muons_CombFit_Lead_Smear_Reso);
   fChain->SetBranchAddress("Muons_CombFit_Sub_Smear_Reso", &Muons_CombFit_Sub_Smear_Reso, &b_Muons_CombFit_Sub_Smear_Reso);
   fChain->SetBranchAddress("Muons_CombFit_Minv_MuMu_Smear_Sigma", &Muons_CombFit_Minv_MuMu_Smear_Sigma, &b_Muons_CombFit_Minv_MuMu_Smear_Sigma);
   fChain->SetBranchAddress("weightr", &weightr, &b_weightr);
   Notify();
}

Bool_t tree_diboson::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void tree_diboson::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t tree_diboson::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef tree_diboson_cxx
